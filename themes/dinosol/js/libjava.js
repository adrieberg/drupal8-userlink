/**
 * @file
 *
 */

(function ($) {

  'use strict';

  /**
   * 
   *
   */
   
	reajustesRedimension();
	
	// Comprobacion svg
	var svgSupport = (window.SVGSVGElement) ? true : false;
	if (!svgSupport){
	    $("body").addClass("no-svg");
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
	
	//Generar cookie
	function createCookie(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}
	
	

	function obtieneAnchoJs(){
		if (document.body)	
			var ancho = (document.body.clientWidth);	
		else	
			var ancho = (window.innerWidth);
		
		var scrollBarWidth = 0;
		if (window.innerWidth)
			scrollBarWidth = window.innerWidth - jQuery("body").width();
		return ancho+scrollBarWidth;
	}
	
	
	function click_links(o) {
		$("#precarga").fadeIn("600", function() {
		window.location.href = o
		});
	}

	function coverFadeOut() {
		$("#precarga").fadeOut("slow", function() {})
	}

		
	function reajustesRedimension(){		
	
		var ancho = obtieneAnchoJs();
	
		
		if (ancho > 767){
		
		}else{
		}
	}

	
	function checkOrientation(){
		var currMode = "";
 
		switch(window.orientation){
 
           case 0:
           currMode = "portrait";
           break;
 
           case -90:
           currMode = "landscape";
           break;
 
           case 90:
           currMode = "landscape";
           break;
 
           case 180:
           currMode = "landscape";
           break;
		}
		return currMode;
	}
	
	var orientationOld = checkOrientation();
	
	$(document).ready(function($) {
		
		$(".flexslider-noticias-intranet .slides img").css("display","none");
		// Menú movil
		$('#responsive-menu-button').sidr({
		   name: 'sidr-main',
		   source: '#main-menu'
		});	
		
		$('.sidr-class-menu-toggle--hide').click(function() {  
				$.sidr('close', 'sidr-main');
 		});
		
		// Efecto fade transicion paginas
		$("a:not([href*='#']):not([target*='_blank']):not([class*='magnific'])").click(function() {	
			var o = this;
			click_links(o);
		});
		
		if($('#precarga').length){ 
			$('#precarga').delay(500).fadeOut(500);
		}
		
		$(window).load(function(){
	
			 // tiny helper function to add breakpoints
            function getGridSize() {
				if (window.innerWidth < 400) 
					return 2;
				else
              return (window.innerWidth < 600) ? 3 :
                     (window.innerWidth < 900) ? 4 : 6;
            }              

			
		  // Slider
		  if($('.flexlider-portada').length > 0){
			  $('.flexlider-portada').flexslider({
				animation: "fade",
				directionNav: true,	
				prevText: "<",
				nextText: ">",   
				controlNav: false,	
				start: function(slider){
				 //$('body').removeClass('loading');
				}
			  });
		  }
		  
		  if($('.flexslider-noticias-intranet').length > 0){
			  
			  $(".flexslider-noticias-intranet img").load(function() {
					//$(".overlay").height($(".flexlider-portada li").height());
					$(this).css("display","none");
					
				});
				$(".flexslider-noticias-intranet li .imagen").each(function(){ 
					$(this).css("background-image", "url("+$("img",this).attr("src")+")");			
					$(this).css("background-size","cover");		
					$(this).css("min-height","15em");
				});
			  
			  $('.flexslider-noticias-intranet').flexslider({
				animation: "fade",
				directionNav: true,	
				prevText: "<",
				nextText: ">",   
				controlNav: false,	
				start: function(slider){
				 //$('body').removeClass('loading');
				}
			  });
		  }
  
		 // Slider con carrusel en una vista 
		 if($('.flexslider-voluntarios').length > 0){
		  $('.flexslider-voluntarios').flexslider({			
			  animation: "slide",
              animationLoop: false,
			  directionNav: true,
			  controlNav:false,
              itemWidth: 112,
              itemMargin: 0,
              minItems: getGridSize(), // use function to pull in initial value
              maxItems: getGridSize() // use function to pull in initial value	
		  });
		 }
		 
		  $('.slider').flexslider({
			selector: ".item-list ul li",
			animation: "fade",
			controlNav: false,
			animationLoop: false,
			slideshow: false,
			sync: ".carousel"
		  });
		}); 
		
		$('.image-popup-no-margins').magnificPopup({
		  type: 'image',
		  closeOnContentClick: true,
		  closeBtnInside: false,
		  fixedContentPos: true,
		  mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
		  image: {
			verticalFit: true
		  },
		  zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		  }
		});		
		
		$(window).bind("resize",function() {
			reajustesRedimension();		

		});	 	
		
		if(readCookie("infCookie")==null){
			//Agregamos codigo html para la ventana de cookies
			if (document.getElementById('capa_cookies')){
			document.getElementById('capa_cookies').innerHTML='<div class="capa_aviso_cookies" id="capa_aviso_cookies"><div class="contenido_aviso_cookies"><p class="titulo_aviso_cookies">Uso de cookies: </p><p>Este sitio web utiliza cookies para que usted tenga la mejor experiencia de usuario. Si contin&uacute;a navegando est&aacute; dando su consentimiento para la aceptaci&oacute;n de las mencionadas cookies y la aceptaci&oacute;n de nuestra <a id="enlace_politica_privacidad_cookies" href="/aviso-legal">pol&iacute;tica de privacidad</a></p><p><a id="aceptar" class="boton_aceptar" href="javascript:void(0);">Aceptar</a></p></div><div class="clearer"></div></div>';	
			
			$('#aceptar').click(function() {
				$('#capa_aviso_cookies').fadeOut('slow', function() {
					createCookie("infCookie",'SI',1825); 
				});
			});
			}
		}


		// Botón volver arriba
		$('a#smooth').click(function() {  
			var $link = $(this);  
			var anchor  = $link.attr('href');  
			$('html, body').stop().animate({  
				scrollTop: $(anchor).offset().top  
			}, 1000);  
			return false;
		});
		
		// Alternador de idiomas
		if ($(".alternador_idioma").length > 0 ){
		  $(".alternador_idioma #block-alternadordeidioma-3").prepend("<div class='idioma_activo'>" + $("html").attr("lang") + "</div>");
		}
		
		$('.alternador_idioma .idioma_activo').click(function() {  
			 
			 if($(window).width() <= 990){
				$(".alternador_idioma ul").fadeIn();
				$('.alternador_idioma .cerrar').css({"display":"block"}); 
			 }else{
				$(".alternador_idioma ul").slideToggle();
			 }
		});
		
		$('.alternador_idioma .cerrar').click(function() {  
			 $(".alternador_idioma ul").fadeOut();
			 $('.alternador_idioma .cerrar').css({"display":"none"});
		});
		
		
		
		// Funcionalidad leer mas y menos
		$('.leer_mas').click(function() {  
			if($(".leer_mas").css("display") == "none"){
				$('.leer_menos').css({"display" : "none"});
				$('.leer_mas').css({"display" : "block"});
			}else{
				$('.leer_menos').css({"display" : "block"});
				$('.leer_mas').css({"display" : "none"});
			}
			
			$(".leer_mas_info").slideToggle("slow");
			
			return false;
		});
		$('.leer_menos').click(function() {  
			$( ".leer_mas_info" ).slideToggle( "slow", function() {
				// Animation complete.
				if($(".leer_menos").css("display") == "none"){
					$('.leer_mas').css({"display" : "none"});
					$('.leer_menos').css({"display" : "block"});
				}else{
					$('.leer_mas').css({"display" : "block"});
					$('.leer_menos').css({"display" : "none"});
				}
				$('html, body').stop().animate({  
					scrollTop: $("#block-tema-ghr-breadcrumbs").offset().top  
				}, 1000); 				
			});
			return false;
		});	
		
	
	});

})(jQuery);