<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\XaiBlock.
 */

namespace Drupal\numerovoluntarios\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;

/**
 * Provides a 'article' block.
 *
 * @Block(
 *   id = "numerovoluntarios_block",
 *   admin_label = @Translation("Número voluntarios block"),
 *   category = @Translation("Custom número voluntarios block example")
 * )
 */
class NumeroVoluntariosBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */

	public function getNumeroVoluntarios(){
	  	$ids = \Drupal::entityQuery('user')
		->condition('status', 1)
		->condition('roles', 'Voluntario')
		->execute();
		$users = User::loadMultiple($ids);
		return count($users);
	}

  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => "<div class='titulo-intranet'><h1>Ya somos <strong>¡".
    	self::getNumeroVoluntarios()." Voluntarios!</strong></h1>".
    	"<div class='intro-intranet'><p>".t("Este espacio es único y tu participación hace la diferencia. Estás invitado a disfrutar de la mejor experiencia como voluntario.  No podemos ayudar a todos pero todos podemos ayudar a alguien.")
    	."</div></div>",
    );
  }
  

}