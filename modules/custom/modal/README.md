Modal
=====

Code: https://adrieberg@bitbucket.org/adrieberg/dinosol.git

Created: Adrie Berg - Ateigh.com

Date: December 2017


What Is This?
-------------

This module is made for Dinosol, and provides functionality for users to 
subscribe to nodes by filling he user reference field of the node with the 
id of the current user.


How To Use
----------

Activate the module to make use of the modal windows to subscribe to a node. 
There are a couple requirements the modules makes.

The href used to open the subscription dialog needs to include the following 
parameters inside the href element:

  class="use-ajax" data-dialog-type="modal"
  
The path that is used for the href needs to have the following format:
  
  /modal/subscribe/ <nodeid>
  
The node that is referred to by <nodeid> needs the be a node which has a field
referencing the user table. This field needs to be called "field_inscritos".
  
Example of a link in plain text opening a subscription dialog to subscribe the 
current user to a node (nid: 4)

  <a class="use-ajax" data-dialog-type="modal" href="/modal/subscribe/4">subscribe</a>

  
DinoSol Specifics
-----------------

In the themes javascript we have a special forward defined for all links that do not 
include the class 'magnific'. To avoid this functionality we need to add the class to
our link:

  <a class="use-ajax magnific" data-dialog-type="modal" href="/modal/subscribe/4">subscribe</a>


