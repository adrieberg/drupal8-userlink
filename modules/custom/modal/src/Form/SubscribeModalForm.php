<?php
/**
 * @file
 * Contains \Drupal\modal\Form\SubscribeModalForm.
 */

namespace Drupal\modal\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class TestModalForm.
 *
 * @package Drupal\modal\Form
 */
class SubscribeModalForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'Subscribe_modal_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $msg_titleconfirm = \Drupal::config('modal.config')->get('message.titleconfirm');
    $msg_confirm = \Drupal::config('modal.config')->get('message.confirm');
    $msg_subscribe = \Drupal::config('modal.config')->get('message.subscribe');
    
    //get the subscription node id (snid) from the desired node
    //which includes a user reference field
    $current_url = \Drupal::service('path.current')->getPath();
    //Set nodeid to the last item in the URL
    $current_url_array = explode("/", $current_url);
    $snid = array_pop((array_slice($current_url_array, -1)));
    $snid_title = Node::load($snid)->get('title')->value;

    $form['node_id'] = array(
      '#type' => 'hidden',
      '#value' => $snid,
      );
    $form['node_title'] = array(
      '#type' => 'hidden',
      '#value' => $snid_title,
      );
    $form['help'] = [
      '#type' => 'item',
      '#markup' => $this->t($msg_confirm) . ' <strong>' . $snid_title . '</strong>',
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t($msg_subscribe),
      '#ajax' => array(
        'callback' => '::open_modal',
      ),
    );

    $form['#title'] = $this->t($msg_titleconfirm);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  public function open_modal(&$form, FormStateInterface $form_state) {
    $node_title = $form_state->getValue('node_title');
    $node_id = $form_state->getValue('node_id');
    
    $msg_confirmed = \Drupal::config('modal.config')->get('message.confirmed');
    $msg_thankyou = \Drupal::config('modal.config')->get('message.thankyou');
    $msg_sorry = \Drupal::config('modal.config')->get('message.sorry');
    $msg_subscribed = \Drupal::config('modal.config')->get('message.subscribed');
    $msg_alreadysubscribed = \Drupal::config('modal.config')->get('message.alreadysubscribed');
    $msg_cannotsubscribe = \Drupal::config('modal.config')->get('message.cannotsubscribe');
    
    $id = $node_id;
    $title = $this->t($msg_confirmed);
    $response = new AjaxResponse();
    if ($id !== NULL) {
      //get current user name
      $current_userid = \Drupal::currentUser()->id();
      $current_user = \Drupal\user\Entity\User::load($current_userid);
      $current_name = $current_user->get('name')->value;

      // Check to see if the user has an account (do not allow anonymous to subscribe)
      if ($current_userid > 0) {
        //Add reference to userid in referred node identified by $key
        $node = Node::load($node_id);
             
        // TODO: field_inscritos is a hard-coded field name, currently used as a node reference
        // to the users table. This field name should be made configurable in the admin section
        // of this module
        ksm(\Drupal::config('modal.config')->get('modal.referencefield'));
        
        //$node_reference_user_field = 'field_subscribe';
        $node_reference_user_field = \Drupal::config('modal.config')->get('modal.referencefield');
        
        // Prepare user message
        $usermessage = '<div class="test-popup-content">';
        $usermessage .= $this->t($msg_thankyou) . ' <strong>' . $current_name . '</strong><br /><br />';
        
        // Check if a field reference to the users table is actually available in the referenced node
        if ($node->hasField($node_reference_user_field)) {

          // Check to see if the user is already subscribed, in which case we should not subscribe the
          // user again
          $current_refvalues = $node->{$node_reference_user_field}->getValue();
          
          // Flatten array with user references so we can look inside to see if the current user is
          // already subscribed
          foreach($current_refvalues as $k=>$v) {
            $refvalues[$k] = $v['target_id'];
          }
          if (!in_array($current_userid, $refvalues)) {
            // Connect user id to the selected node
            $node->get($node_reference_user_field)[] = ['target_id' => $current_userid];
            // Save the updated node
            $node->save();
            
            $usermessage .= $this->t($msg_subscribed) . ' <strong>' . $node_title . '</strong>';
          }
          else {
            $usermessage .= $this->t($msg_alreadysubscribed) . ' <strong>' . $node_title . '</strong>';
          }
        }
        else {
          $usermessage .= $this->t($msg_cannotsubscribe) . ' <strong>' . $node_title . '</strong>';
          $logmessage = "Notice: Field with user reference not found: " . $node_reference_user_field;
          \Drupal::logger('modal')->notice($logmessage);
        }

        // Prepare user message
        $usermessage .= '</div>';
     
        $options = array(
          'dialogClass' => 'popup-dialog-class',
          'width' => '300',
          'height' => '200',
        );
        $response->addCommand(new OpenModalDialogCommand($title, $usermessage, $options));
      } 
      else {
        //anonymous user, should request login first, write log message
        $logmessage = "Notice: Attempt to subscribe with anonymous user, check node access.";
        \Drupal::logger('modal')->notice($logmessage);
      }
    }
    else {
      $usermessage = '<div class="test-popup-content">';
      $usermessage .= $this->t($msg_sorry) . '<br /><br />';
      $usermessage .= $this->t($msg_cannotsubscribe);
      $usermessage .= '</div>';
      $response->addCommand(new OpenModalDialogCommand($title, $usermessage));
    }
    return $response;
  }
}
